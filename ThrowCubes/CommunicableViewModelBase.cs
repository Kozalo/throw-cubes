﻿using GalaSoft.MvvmLight;

namespace ThrowCubes
{
    public abstract class CommunicableViewModelBase : ViewModelBase
    {
        protected ViewModelsCommunicator Communicator;

        protected CommunicableViewModelBase(ViewModelsCommunicator communicator)
        {
            Communicator = communicator;
        }
    }
}
