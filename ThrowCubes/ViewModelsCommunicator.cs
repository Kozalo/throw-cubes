﻿using System.Collections.ObjectModel;
using ThrowCubes.BL;

namespace ThrowCubes
{
    public class ViewModelsCommunicator
    {
        public ExperimentsInfoModelView ExperimentsInfoModelView { get; }
        public FrequencyTableViewModel FrequencyTableViewModel { get; }
        public AutoExperimentsViewModel AutoExperimentsViewModel { get; }

        public ObservableCollection<PossibleValueInfo> ExperimentResultCollection => FrequencyTableViewModel.PossibleValues;

        public ViewModelsCommunicator()
        {
            ExperimentsInfoModelView = new ExperimentsInfoModelView(this);
            FrequencyTableViewModel = new FrequencyTableViewModel(this);
            AutoExperimentsViewModel = new AutoExperimentsViewModel(this);
        }

        internal void CalculationStarted()
        {
            ExperimentsInfoModelView.IsRunning = true;
            AutoExperimentsViewModel.IsRunning = true;
        }

        internal void CalculationFinished()
        {
            ExperimentsInfoModelView.IsRunning = false;
            AutoExperimentsViewModel.IsRunning = false;
        }
    }
}
