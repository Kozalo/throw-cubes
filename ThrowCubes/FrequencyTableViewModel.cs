﻿using System.Collections.ObjectModel;
using ThrowCubes.BL;

namespace ThrowCubes
{
    public class FrequencyTableViewModel : CommunicableViewModelBase
    {
        public FrequencyTableViewModel(ViewModelsCommunicator communicator)
            : base(communicator)
        { }


        public ObservableCollection<PossibleValueInfo> PossibleValues { get; } = new ObservableCollection<PossibleValueInfo>();
    }
}
