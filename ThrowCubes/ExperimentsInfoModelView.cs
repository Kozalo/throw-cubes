﻿using System;
using ThrowCubes.BL;

namespace ThrowCubes
{
    public class ExperimentsInfoModelView : CommunicableViewModelBase
    {
        public ExperimentsInfoModelView(ViewModelsCommunicator communicator)
            : base(communicator)
        { }

        #region Properties

        private uint _cubeFaceCount;
        private uint _cubeCount;
        private uint _throwCount;

        private uint _executedThrows;
        private TimeSpan _consumedTime;

        private bool _isRunning;


        public uint CubeFaceCount
        {
            get { return _cubeFaceCount; }
            set { Set(ref _cubeFaceCount, value, broadcast: true); }
        }

        public uint CubeCount
        {
            get { return _cubeCount; }
            set { Set(ref _cubeCount, value, broadcast: true); }
        }

        public uint ThrowCount
        {
            get { return _throwCount; }
            set { Set(ref _throwCount, value, broadcast: true); }
        }


        public uint ExecutedThrows
        {
            get { return _executedThrows; }
            private set { Set(ref _executedThrows, value); }
        }

        public TimeSpan ConsumedTime
        {
            get { return _consumedTime; }
            private set { Set(ref _consumedTime, value); }
        }


        public bool IsRunning
        {
            get { return _isRunning; }
            internal set { Set(ref _isRunning, value, broadcast: true); }
        }

        #endregion

        #region Commands

        private AutoRelayCommand _runThrowsCommand;

        public AutoRelayCommand RunThrowsCommand
        {
            get
            {
                if (_runThrowsCommand == null)
                {
                    _runThrowsCommand = new AutoRelayCommand(RunThrows, () =>
                        CubeFaceCount > 0 && CubeCount > 0 && ThrowCount > 0 &&
                        !IsRunning &&
                        CubeFaceCount * CubeCount <= 1000000
                    );
                    _runThrowsCommand.DependsOn(() => CubeFaceCount);
                    _runThrowsCommand.DependsOn(() => CubeCount);
                    _runThrowsCommand.DependsOn(() => ThrowCount);
                    _runThrowsCommand.DependsOn(() => IsRunning);
                }

                return _runThrowsCommand;
            }
        }

        #endregion


        private async void RunThrows()
        {
            Communicator.CalculationStarted();

            var possibleValues = Communicator.ExperimentResultCollection;
            possibleValues.Clear();

            var finalResult = await ExperimentExecutor.ExecuteAsync(CubeFaceCount, CubeCount, ThrowCount, new Progress<ExperimentInfo>(
                experimentsInfo =>
                {
                    ExecutedThrows = experimentsInfo.ThrowCount;
                    ConsumedTime = experimentsInfo.ConsumedTime;
                    
                    if (possibleValues.Count == 0)
                        foreach (var exp in experimentsInfo.PossibleValues)
                            possibleValues.Add(exp);
                })
            );

            ExecutedThrows = finalResult.ThrowCount;
            ConsumedTime = finalResult.ConsumedTime;

            possibleValues.Clear();
            foreach (var exp in finalResult.PossibleValues)
                possibleValues.Add(exp);

            Communicator.CalculationFinished();
        }
    }
}