﻿using System;
using System.Collections.ObjectModel;
using ThrowCubes.BL;

namespace ThrowCubes
{
    public class AutoExperimentsViewModel : CommunicableViewModelBase
    {
        public AutoExperimentsViewModel(ViewModelsCommunicator communicator)
            : base(communicator)
        { }

        #region Properties

        private uint _cubeFaceCount;
        private uint _cubeCount;
        private uint _throwCount;

        private int _cubeFaceCountStep;
        private int _cubeCountStep;
        private int _throwCountStep;

        private uint _experimentCount;

        private bool _isRunning;

        private ObservableCollection<ExperimentParams> _experimentsParams = new ObservableCollection<ExperimentParams>();
        

        public int SelectedExperimentIndex
        {
            set
            {
                var experimentTable = Communicator.ExperimentResultCollection;
                experimentTable.Clear();
                foreach (var possibleValue in ExperimentInfos[value].PossibleValues)
                    experimentTable.Add(possibleValue);
            }
        }


        public uint CubeFaceCount
        {
            get { return _cubeFaceCount; }
            set { Set(ref _cubeFaceCount, value, broadcast: true); }
        }

        public uint CubeCount
        {
            get { return _cubeCount; }
            set { Set(ref _cubeCount, value, broadcast: true); }
        }

        public uint ThrowCount
        {
            get { return _throwCount; }
            set { Set(ref _throwCount, value, broadcast: true); }
        }


        public int CubeFaceCountStep
        {
            get { return _cubeFaceCountStep; }
            set { Set(ref _cubeFaceCountStep, value, broadcast: true); }
        }

        public int CubeCountStep
        {
            get { return _cubeCountStep; }
            set { Set(ref _cubeCountStep, value, broadcast: true); }
        }

        public int ThrowCountStep
        {
            get { return _throwCountStep; }
            set { Set(ref _throwCountStep, value, broadcast: true); }
        }


        public uint ExperimentCount
        {
            get { return _experimentCount; }
            set { Set(ref _experimentCount, value, broadcast: true); }
        }


        public ObservableCollection<ExperimentParams> ExperimentsParams
        {
            get { return _experimentsParams; }
            set { Set(ref _experimentsParams, value, broadcast: true); }
        }

        public bool IsRunning
        {
            get { return _isRunning; }
            internal set { Set(ref _isRunning, value, broadcast: true); }
        }

        public ExperimentInfo[] ExperimentInfos { get; private set; }

        #endregion

        #region Commands

        private AutoRelayCommand _runExperimentsCommand;

        public AutoRelayCommand RunExperimentsCommand
        {
            get
            {
                if (_runExperimentsCommand == null)
                {
                    _runExperimentsCommand = new AutoRelayCommand(RunExperiments, () =>
                    {
                        var n = ExperimentCount - 1;
                        var finalCubeFaceCount = CubeFaceCount + CubeFaceCountStep * n;
                        var finalCubeCount = CubeCount + CubeCountStep * n;
                        var finalThrowCount = ThrowCount + ThrowCountStep * n;

                        return !IsRunning
                               && CubeFaceCount > 0 && CubeCount > 0 && ThrowCount > 0 && ExperimentCount > 0
                               && finalCubeFaceCount > 0 && finalCubeCount > 0 && finalThrowCount > 0
                               && finalCubeFaceCount * finalCubeCount <= 1000000;
                    });

                    _runExperimentsCommand.DependsOn(() => CubeFaceCount);
                    _runExperimentsCommand.DependsOn(() => CubeCount);
                    _runExperimentsCommand.DependsOn(() => ThrowCount);
                    _runExperimentsCommand.DependsOn(() => ExperimentCount);
                    _runExperimentsCommand.DependsOn(() => CubeFaceCountStep);
                    _runExperimentsCommand.DependsOn(() => CubeCountStep);
                    _runExperimentsCommand.DependsOn(() => ThrowCountStep);
                    _runExperimentsCommand.DependsOn(() => IsRunning);
                }

                return _runExperimentsCommand;
            }
        }

        #endregion

        private async void RunExperiments()
        {
            Communicator.CalculationStarted();

            var cubeFaceCount = CubeFaceCount;
            var cubeCount = CubeCount;
            var throwCount = ThrowCount;

            ExperimentsParams.Clear();
            ExperimentInfos = new ExperimentInfo[ExperimentCount];

            for (uint i = 0; i < ExperimentCount; i++)
            {
                var experimentInfo = await ExperimentExecutor.ExecuteAsync(cubeFaceCount, cubeCount, throwCount, new Progress<ExperimentInfo>());

                ExperimentsParams.Add(new ExperimentParams(i + 1, cubeFaceCount, cubeCount, throwCount, experimentInfo.ConsumedTime));
                ExperimentInfos[i] = experimentInfo;

                cubeFaceCount = (uint) (cubeFaceCount + CubeFaceCountStep);
                cubeCount = (uint)(cubeCount + CubeCountStep);
                throwCount = (uint)(throwCount + ThrowCountStep);
            }

            Communicator.CalculationFinished();
        }
    }

    public struct ExperimentParams
    {
        public uint ID { get; }
        public uint CubeFaceCount { get; }
        public uint CubeCount { get; }
        public uint ThrowCount { get; }
        public TimeSpan ConsumedTime { get; set; }

        public ExperimentParams(uint id, uint cubeFaceCount, uint cubeCount, uint throwCount, TimeSpan consumedTime)
        {
            ID = id;
            CubeFaceCount = cubeFaceCount;
            CubeCount = cubeCount;
            ThrowCount = throwCount;
            ConsumedTime = consumedTime;
        }
    }
}
