Throw Cubes / Бросаем кубики
============================

A small program in C# that is supposed to simulate a process of throwing the dice (and even several dices simultaneously) again and again to collect some statistic information about these throws. Written using MVVM as a main pattern and WPF with XAML for UI declaration. Requires **.NET Framework 4.5.2** to work.

Небольшая программа на C#, предназначенная для эмуляции процесса бросания различных наборов игральных костей с целью сбора статистической информации. Написана с использованием MVVM в качестве основного паттерна и WPF с XAML для описания интерфейса. Для работы требует **.NET Framework 4.5.2**.
