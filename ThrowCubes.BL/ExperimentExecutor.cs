﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using GalaSoft.MvvmLight;

namespace ThrowCubes.BL
{
    public class PossibleValueInfo : ObservableObject
    {
        public uint PossibleValue { get; }
        public uint BeenThrown { get; set; }
        private readonly uint _throwCount;

        private PossibleValueInfo(uint possibleValue, uint throwCount)
        {
            PossibleValue = possibleValue;
            _throwCount = throwCount;
        }

        public static PossibleValueInfo[] GetPossibleValues(uint first, uint last, uint throwCount)
        {
            var instances = new PossibleValueInfo[last - first + 1];
            for (var i = first; i <= last; i++)
                instances[i - first] = new PossibleValueInfo(i, throwCount);
            return instances;
        }

        public void ThatIsIt()
        {
            BeenThrown++;
            RaisePropertyChanged(() => BeenThrown);
            RaisePropertyChanged(() => Frequency);
        }

        public double Frequency => (double) BeenThrown / _throwCount;
    }

    public class ExperimentInfo : ObservableObject
    {
        public PossibleValueInfo[] PossibleValues { get; }
        public TimeSpan ConsumedTime { get; set; }
        public uint ThrowCount { get; private set; }

        private readonly uint _arrayOffset;

        public ExperimentInfo(uint cubeFaceCount, uint cubeCount, uint throwCount)
        {
            PossibleValues = PossibleValueInfo.GetPossibleValues(cubeCount, cubeCount * cubeFaceCount, throwCount);
            _arrayOffset = cubeCount;
        }

        public void AddThrow(uint value)
        {
            PossibleValues[value - _arrayOffset].ThatIsIt();

            ThrowCount++;
            RaisePropertyChanged(() => ThrowCount);
        }
    }

    public class ExperimentExecutor
    {
        public static ExperimentInfo Execute(uint cubeFaceCount, uint cubeCount, uint throwCount)
        {
            var stopwatch = new Stopwatch();
            var experiment = new ExperimentInfo(cubeFaceCount, cubeCount, throwCount);

            stopwatch.Start();

            for (var i = 0; i < throwCount; i++)
            {
                uint totalValue = 0;

                for (var j = 0; j < cubeCount; j++)
                {
                    var cube = new Dice(cubeFaceCount);
                    var diceValue = cube.Roll();

                    totalValue += diceValue;
                }

                experiment.AddThrow(totalValue);
            }

            stopwatch.Stop();

            experiment.ConsumedTime = stopwatch.Elapsed;
            return experiment;
        }

        public static Task<ExperimentInfo> ExecuteAsync(uint cubeFaceCount, uint cubeCount, uint throwCount, IProgress<ExperimentInfo> progress)
        {
            return Task.Run(() =>
            {
                const uint notifyIntervalInMilliseconds = 100;

                var startTime = DateTime.UtcNow;
                var notifyTime = DateTime.UtcNow;
                var experiment = new ExperimentInfo(cubeFaceCount, cubeCount, throwCount);

                for (var i = 0; i < throwCount; i++)
                {
                    uint totalValue = 0;

                    for (var j = 0; j < cubeCount; j++)
                    {
                        var cube = new Dice(cubeFaceCount);
                        var diceValue = cube.Roll();

                        totalValue += diceValue;
                    }

                    experiment.AddThrow(totalValue);
                    experiment.ConsumedTime = DateTime.UtcNow - startTime;

                    var timeSpan = DateTime.UtcNow - notifyTime;
                    if (timeSpan.TotalMilliseconds > notifyIntervalInMilliseconds)
                    {
                        notifyTime = DateTime.UtcNow;
                        progress.Report(experiment);
                    }
                }

                return experiment;
            });
        }
    }
}
