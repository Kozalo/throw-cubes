﻿using System;

namespace ThrowCubes.BL
{
    public class Dice
    {
        private static readonly Random Random = new Random();
        private readonly uint _sides;

        public Dice(uint sides)
        {
            if (sides < 1)
                throw new ArgumentException("Count of sides cannot be less than 1.");

            _sides = sides;
        }

        public uint Roll()
        {
            return (uint) Random.Next(1, (int) _sides + 1);
        }
    }
}